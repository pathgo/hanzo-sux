import express, { Router, Express } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import errorRequestHandler from './error/error_request_handler';

const CreateExpressApp = (router: Router) : Express => {
  const app = express();
  app.use(cors());
  app.use(bodyParser.json());
  app.use(router);
  app.use(errorRequestHandler);
  return app;
};

export default CreateExpressApp;
