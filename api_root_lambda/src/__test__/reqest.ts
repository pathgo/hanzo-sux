import CreateExpressApp from '../app';
import PublicRouter from '../router/public';

export const PublicTestApp = CreateExpressApp(PublicRouter);
