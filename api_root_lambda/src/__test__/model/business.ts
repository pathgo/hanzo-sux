import Business from '../../model/business';
import { BusinessModel, CreateBusinessProps } from '../../model/business/types';

export default class TestBusinessModel {
  static async Create(props: Partial<CreateBusinessProps> = {}) : Promise<BusinessModel> {
    const {
      ownerId = '1',
      businessName = `Test Business ${`${Math.random()}`.substr(0, 5)}`,
      address = {
        streetAddress: '1111 1st avenue',
        state: 'NY',
        city: 'New York',
        zipcode: 11111,
      },
      floorplanConfig = {
        rowCount: 100,
        columnCount: 100,
      },
    } = props;
    const business = await Business.Create({
      ownerId,
      businessName,
      address,
      floorplanConfig,
    });
    return business;
  }
}
