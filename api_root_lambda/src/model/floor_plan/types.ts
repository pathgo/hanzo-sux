export enum FloorGridNodeType {
  Wall = 'Wall',
  Path = 'Path',
  Waypoint = 'Waypoint',
}

export type WaypointGridNodeProps = {
  title: string;
};

export type FloorPlanGridNode = {
  nodeType: FloorGridNodeType;
  nodeProps?: WaypointGridNodeProps;
};

export type FloorPlanArrayIndex = {
  floorTitle: string,
  floorGrid: Array<Array<FloorPlanGridNode>>
}

export type FloorPlanModel = Array<FloorPlanArrayIndex>
