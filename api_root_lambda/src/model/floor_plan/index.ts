import FirebaseConfig from '../../config/firebase';
import Firestore from '../../service/firebase/firestore';
import BusinessFirebaseParser from '../../util/firebase_parser';
import {
  FloorGridNodeType,
  FloorPlanGridNode,
  FloorPlanModel,
} from './types';

export default class FloorPlan {
  static GenerateFloorGrid = (
    rowCount: number,
    columnCount: number,
  ): Array<Array<FloorPlanGridNode>> => {
    const grid: Array<Array<FloorPlanGridNode>> = [];
    for (let row = 0; row < rowCount; row += 1) {
      grid.push([]);
      for (let col = 0; col < columnCount; col += 1) {
        grid[row].push({
          nodeType: FloorGridNodeType.Path,
        });
      }
    }
    return grid;
  };

  static async UpdateFloorplan(
    businessId: string,
    updatedFloorplan: FloorPlanModel,
  ): Promise<void> {
    const {
      floorplanWithoutWaypoints,
      waypoints,
    } = BusinessFirebaseParser.SplitFloorplanWaypoint(updatedFloorplan);
    const stringifiedFloorplan = floorplanWithoutWaypoints.map((entry) => JSON.stringify(entry));
    await Firestore.collection(FirebaseConfig.CollectionName.Business)
      .doc(businessId)
      .update({
        floorPlan: stringifiedFloorplan,
        waypoints,
      });
  }
}
