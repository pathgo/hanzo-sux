import FirebaseConfig from '../../config/firebase';
import Firestore from '../../service/firebase/firestore';
import BusinessFirebaseParser from '../../util/firebase_parser';
import FloorPlan from '../floor_plan';
import { FloorPlanArrayIndex } from '../floor_plan/types';
import {
  CreateBusinessProps,
  BusinessModel,
  FirebaseBusinessFloorPlanCollection,
  BusinessModelSummary,
} from './types';

export default class Business {
  static async Create(data: CreateBusinessProps): Promise<BusinessModel> {
    try {
      const {
        ownerId,
        businessName,
        address,
        floorplanConfig: { rowCount, columnCount },
      } = data;
      console.log('Creating Business Document');
      const doc = Firestore.collection(
        FirebaseConfig.CollectionName.Business,
      ).doc();
      const floorGrid = FloorPlan.GenerateFloorGrid(rowCount, columnCount);
      const groundFloor: FloorPlanArrayIndex = { floorTitle: 'G', floorGrid };
      const firebaseEntry: Omit<FirebaseBusinessFloorPlanCollection, 'id'> = {
        ownerId,
        businessName,
        zipcode: address.zipcode,
        address: {
          streetAddress: address.streetAddress,
          city: address.city,
          state: address.state,
        },
        waypoints: [],
        floorPlan: [JSON.stringify(groundFloor)],
      };
      console.log('Adding to firebase');
      await doc.set(firebaseEntry);
      return {
        id: doc.id,
        businessName,
        address,
        floorPlan: [groundFloor],
      };
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  static async FindByTitle(title: string): Promise<Array<BusinessModel>> {
    const results = await Firestore.collection(
      FirebaseConfig.CollectionName.Business,
    )
      .where('businessName', '==', title)
      .get();
    if (results.docs.length === 0) return [];
    const businesses = results.docs.map(
      (entry) => {
        const fbDoc = entry.data() as Omit<FirebaseBusinessFloorPlanCollection, 'id'>;
        const model: BusinessModel = {
          id: entry.id,
          businessName: fbDoc.businessName,
          address: {
            ...fbDoc.address,
            zipcode: fbDoc.zipcode,
          },
          floorPlan: BusinessFirebaseParser.ParseFloorPlan({
            id: entry.id,
            ...fbDoc,
          }),
        };
        return model;
      },
    );
    return businesses;
  }

  static async FindBusinessById(id: string): Promise<BusinessModel> {
    const result = await Firestore.collection(
      FirebaseConfig.CollectionName.Business,
    )
      .doc(id)
      .get();
    const fbDoc = result.data() as FirebaseBusinessFloorPlanCollection;
    if (fbDoc === undefined) throw new Error('Invalid ID');
    const model: BusinessModel = {
      id,
      businessName: fbDoc.businessName,
      address: {
        ...fbDoc.address,
        zipcode: fbDoc.zipcode,
      },
      floorPlan: BusinessFirebaseParser.ParseFloorPlan(fbDoc),
    };
    return model;
  }

  static async DeleteBusiness(businessId: string) : Promise<void> {
    await Firestore
      .collection(FirebaseConfig.CollectionName.Business)
      .doc(businessId)
      .delete();
  }

  static async FindBusinessByZip(zip: number): Promise<Array<BusinessModelSummary>> {
    const results = await Firestore.collection(
      FirebaseConfig.CollectionName.Business,
    )
      .where('zipcode', '==', zip)
      .get();
    if (results.docs.length === 0) return [];
    const businesses = results.docs.map(
      (entry) => {
        const fbDoc = entry.data() as FirebaseBusinessFloorPlanCollection;
        const model: BusinessModelSummary = {
          id: entry.id,
          businessName: fbDoc.businessName,
          address: {
            ...fbDoc.address,
            zipcode: fbDoc.zipcode,
          },
        };
        return model;
      },
    );
    return businesses;
  }

  static async GetBusinessesForUser(userId: string): Promise<Array<BusinessModelSummary>> {
    const results = await Firestore.collection(
      FirebaseConfig.CollectionName.Business,
    )
      .where('ownerId', '==', userId)
      .get();
    if (results.docs.length === 0) return [];
    const businesses = results.docs.map(
      (entry) => {
        const fbDoc = entry.data() as Omit<FirebaseBusinessFloorPlanCollection, 'id'>;
        const model: BusinessModelSummary = {
          id: entry.id,
          businessName: fbDoc.businessName,
          address: {
            ...fbDoc.address,
            zipcode: fbDoc.zipcode,
          },
        };
        return model;
      },
    );
    return businesses;
  }
}
