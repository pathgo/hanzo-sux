import { FloorPlanModel, FloorPlanGridNode } from '../floor_plan/types';

export type BusinessAddress = {
  streetAddress: string;
  unit?: string;
  state: string;
  city: string;
  zipcode: number;
};

export type FloorPlanConfig = {
  rowCount: number;
  columnCount: number;
};

export type CreateBusinessProps = {
  ownerId: string,
  businessName: string;
  address: BusinessAddress;
  floorplanConfig: FloorPlanConfig;
};

export interface BusinessModelAttributes {
  businessName: string;
  address: BusinessAddress;
  floorPlan: FloorPlanModel
}

export interface BusinessModel extends BusinessModelAttributes{
  id: string;
}

export type BusinessModelSummary = Omit<BusinessModel, 'floorPlan'>

export type FloorPlanIndex = {
  floorName: string;
  floorGrid: Array<Array<FloorPlanGridNode>>;
};

export type FirebaseSelectFloorFloorPlan = {
  rowCount: number

}

export type FirebaseBusinessFloorPlanEntry = {
  floorOrder: Array<string>
}

export type FirebaseBusinessEntry = {
  businessName: string;
  address: BusinessAddress;
  floorPlan: Array<string>
};

export interface FirebaseBusinessFloorPlanWaypoint {
  floorTitle: string,
  title: string,
  row: number,
  column: number
}

export interface FirebaseBusinessFloorPlanCollection {
  id: string,
  ownerId: string,
  businessName: string,
  zipcode: number,
  address: {
    streetAddress: string,
    city: string,
    state: string
  },
  waypoints: Array<FirebaseBusinessFloorPlanWaypoint>,
  floorPlan: Array<string> // JSON
}
