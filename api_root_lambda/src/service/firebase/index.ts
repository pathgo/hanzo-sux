import * as admin from 'firebase-admin';
import FirebaseConfig from '../../config/firebase';
import AppStage, { CURRENT_APP_STAGE } from '../../constant/app_stage';
import logger from '../../util/logger';

const LOG_TAG = 'Firebase';

const getApp = () => {
  if (CURRENT_APP_STAGE === AppStage.Test) return admin.initializeApp();
  logger.info(LOG_TAG, `Firebase Project: ${FirebaseConfig.Credential.projectId}`);
  return admin.initializeApp({
    credential: admin.credential.cert(FirebaseConfig.Credential),
  });
};

const FirebaseApp = getApp();

FirebaseApp.firestore();

export default FirebaseApp;
