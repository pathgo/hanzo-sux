/* eslint-disable consistent-return */
import FirebaseConfig from '../../../config/firebase';
import { FirebaseBusinessEntry } from '../../../model/business/types';

type CollectionStorageType = Record<string, Record<string, unknown>>;

const CollectionStorage: CollectionStorageType = {
  [FirebaseConfig.CollectionName.Business]: {},
};

const MockFirestore = {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  collection(collectionName: string) {
    return {
      doc: (id = `${Math.random()}`) => ({
        id,
        set: async (data: unknown) => {
          if (collectionName in CollectionStorage) {
            CollectionStorage[collectionName][id] = data;
          }
        },
        get: async () => ({
          data: () => CollectionStorage[collectionName][id],
        }),
      }),
      where: (field: string, _: string, value: unknown) => ({
        get: async () => {
          const docs: { id: string; data: () => FirebaseBusinessEntry; }[] = [];
          Object.keys(CollectionStorage[collectionName]).forEach(
            (businessId) => {
              const entry = CollectionStorage[collectionName][
                businessId
              ] as FirebaseBusinessEntry;
              if (entry.businessName === value) {
                docs.push({
                  id: businessId,
                  data: () => entry,
                });
              }
            },
          );
          return {
            docs,
          };
        },
      }),
    };
  },
};

export default MockFirestore;
