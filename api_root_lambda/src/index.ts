import serverlessExpress from '@vendia/serverless-express';
import CreateExpressApp from './app';
import AuthRouter from './router/auth';
import PublicRouter from './router/public';

export const PublicAPIHandler = serverlessExpress({
  app: CreateExpressApp(PublicRouter),
});

export const ProtectedAPIHandler = serverlessExpress({
  app: CreateExpressApp(AuthRouter),
});
