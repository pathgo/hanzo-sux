export interface RequestErrorProps {
  status: number
  message?: string
}

export default class RequestError extends Error {
  status: number

  date: Date

  constructor(props: RequestErrorProps) {
    super();
    this.status = props.status;
    if (props.message) this.message = props.message;
    this.date = new Date();
    this.name = 'RequestError';
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, RequestError);
    }
  }
}
