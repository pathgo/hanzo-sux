export default class SetupError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'SetupError';
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, SetupError);
    }
  }

  static MissingENV(variableName: string) : SetupError {
    return new SetupError(`Missing Environment Variable: ${variableName}`);
  }
}
