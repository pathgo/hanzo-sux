import dotenv from 'dotenv';
import SetupError from '../error/setup_error';

dotenv.config();

export const GetENVOrThrow = (varname: string) : string => {
  if (process.env[varname] === undefined) throw SetupError.MissingENV(varname);
  return process.env[varname] as string;
};
