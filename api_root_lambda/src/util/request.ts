import { getCurrentInvoke } from '@vendia/serverless-express';

export const GetCurrentUserId = () : string => {
  const { event } = getCurrentInvoke();
  return event.requestContext.authorizer.claims.sub;
};
