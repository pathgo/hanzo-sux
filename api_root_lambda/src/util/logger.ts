import { TestConfig } from '../config/test';
import AppStage, { CURRENT_APP_STAGE } from '../constant/app_stage';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type LogMessageFN = (tag: string, message: any) => void;

type LogFN = LogMessageFN;

export interface ILogger {
  local: LogFN
  info: LogFN
  warn: LogFN
  error: LogFN
}

class LoggerImpl implements ILogger {
  // TODO: Handle Object logs
  local : LogMessageFN = (tag, message) => {
    if (CURRENT_APP_STAGE === AppStage.Test && TestConfig.LOGGING_ENABLED) return;
    console.log(tag, message);
  };

  info : LogMessageFN = (tag, message) => {
    if (CURRENT_APP_STAGE === AppStage.Test && !TestConfig.LOGGING_ENABLED) return;
    console.log(tag, message);
  };

  warn: LogMessageFN = (tag, message) => {
    if (CURRENT_APP_STAGE === AppStage.Test && !TestConfig.LOGGING_ENABLED) return;
    console.log(tag, message);
  };

  error : LogMessageFN = (tag, message) => {
    if (CURRENT_APP_STAGE === AppStage.Test && !TestConfig.LOGGING_ENABLED) return;
    console.log(tag, message);
  };
}

function getLogger() : ILogger {
  // TODO: Create appropriate logger based on environment
  return new LoggerImpl();
}

const logger = getLogger();

export default logger;
