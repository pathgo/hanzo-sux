import { FloorGridNodeType, FloorPlanArrayIndex, FloorPlanModel } from '../model/floor_plan/types';
import { FirebaseBusinessFloorPlanCollection, FirebaseBusinessFloorPlanWaypoint } from '../model/business/types';

export type SplitFloorplanWaypointOutput = {
  floorplanWithoutWaypoints: FloorPlanModel,
  waypoints: Array<FirebaseBusinessFloorPlanWaypoint>
}

export default class BusinessFirebaseParser {
  static ParseFloorPlan(doc: FirebaseBusinessFloorPlanCollection) : FloorPlanModel {
    const { floorPlan: floorplanJSONString } = doc;
    const floorTitleToArrayIndex : Record<string, number> = {};
    // Floor Plan Without Waypoints
    const floorPlan: FloorPlanModel = floorplanJSONString.map(
      (entry, index) => {
        const floorGrid = JSON.parse(entry) as FloorPlanArrayIndex;
        floorTitleToArrayIndex[floorGrid.floorTitle] = index;
        return floorGrid;
      },
    );
    // Add Waypoint to floor plan
    doc.waypoints.forEach((waypoint) => {
      const {
        floorTitle, row, column, title,
      } = waypoint;
      const index = floorTitleToArrayIndex[floorTitle];
      if (index !== undefined) {
        floorPlan[index].floorGrid[row][column] = {
          nodeType: FloorGridNodeType.Waypoint,
          nodeProps: {
            title,
          },
        };
      }
    });
    return floorPlan;
  }

  static SplitFloorplanWaypoint(floorplan: FloorPlanModel) : SplitFloorplanWaypointOutput {
    const waypoints : Array<FirebaseBusinessFloorPlanWaypoint> = [];
    const floorplanWithoutWaypoints = floorplan.map((floor) => {
      const { floorTitle, floorGrid } = floor;

      // Removes Waypoint from grid
      const updatedFloorGrid = floorGrid.map((row, rowIndex) => row.map((node, colIndex) => {
        if (node.nodeType === FloorGridNodeType.Waypoint) {
          waypoints.push({
            floorTitle,
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            title: node.nodeProps!.title,
            row: rowIndex,
            column: colIndex,
          });
          return {
            nodeType: FloorGridNodeType.Path,
          };
        }
        return node;
      }));

      return {
        floorTitle,
        floorGrid: updatedFloorGrid,
      };
    });

    return {
      waypoints,
      floorplanWithoutWaypoints,
    };
  }
}
