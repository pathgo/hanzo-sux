import { FloorGridNodeType, FloorPlanGridNode } from '../model/floor_plan/types';

function ShortestPath(
  grid: FloorPlanGridNode[][],
  start: [number, number],
  end: [number, number],
): number[][] {
  const indexKey = (row: number, col: number) => `${row}-${col}`;

  const indexKeyToRowCol = (key: string): [number, number] => {
    const [row, col] = key.split('-').map((strNum) => parseInt(strNum, 10));
    return [row, col];
  };

  const inInBound = (
    row: number, col: number,
  ) => row < grid.length && row >= 0 && col >= 0 && col < grid[row].length;

  const getNeighbors = (row: number, col: number): Array<[number, number]> => {
    const possibleNeighbors = [
      [row + 1, col],
      [row - 1, col],
      [row, col + 1],
      [row, col - 1],
      // [row + 1, col + 1],
      // [row + 1, col - 1],
      // [row - 1, col - 1],
      // [row - 1, col + 1],
    ].filter((entry) => inInBound(entry[0], entry[1])) as Array<
      [number, number]
    >;
    return possibleNeighbors;
  };

  // TODO: Implement as a LinkedList queue for better performance
  const queue = new Array<[number, number]>();
  const seen = new Set<string>();
  seen.add(indexKey(start[0], start[1]));
  queue.push(start);
  const visitedFrom: Record<string, string | null> = {
    [indexKey(start[0], start[1])]: null,
  };
  while (queue.length > 0) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const [currentRow, currentCol] = queue.shift()!;
    if (currentRow === end[0] && currentCol === end[1]) break; // Found
    const parentKey = indexKey(currentRow, currentCol);
    const neighbors = getNeighbors(currentRow, currentCol);
    // eslint-disable-next-line no-restricted-syntax
    for (const [neighborRow, neighborCol] of neighbors) {
      const neighborKey = indexKey(neighborRow, neighborCol);
      if (!seen.has(neighborKey)) {
        seen.add(neighborKey);
        visitedFrom[neighborKey] = parentKey;
        if (grid[neighborRow][neighborCol].nodeType === FloorGridNodeType.Path) {
          queue.push([neighborRow, neighborCol]);
        }
      }
    }
  }
  const path: Array<[number, number]> = [];
  let currentNode = end;
  while (currentNode !== null) {
    path.push(currentNode);
    if (currentNode[0] === start[0] && currentNode[1] === start[1]) break;
    const currentKey = indexKey(currentNode[0], currentNode[1]);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const parentKey = visitedFrom[currentKey]!;
    if (parentKey === null) break;
    currentNode = indexKeyToRowCol(parentKey);
  }
  return path;
}

export default ShortestPath;
