import { RequestHandler } from 'express';
import Joi, { ValidationError } from 'joi';
import RequestError from '../../error/request_error';
import Business from '../../model/business';

const joiString = (varDisplayName: string) => Joi.string().required().messages({
  'any.required': `${varDisplayName} is required`,
  'string.empty': `${varDisplayName} cannot be empty`,
});

const CreateBusinessSchema = Joi.object({
  ownerId: joiString('Owner Id'), // TODO: Convert to using authorizer
  businessName: joiString('Business Name'),
  address: Joi.object({
    streetAddress: joiString('Street Address'),
    unit: Joi.string().optional(),
    state: joiString('State').length(2),
    city: joiString('City'),
    zipcode: Joi.number().required(),
  }).required(),
  floorplanConfig: Joi.object({
    rowCount: Joi.number().required(),
    columnCount: Joi.number().required(),
  }).required(),
});

const CreateBusinessRequestHandler: RequestHandler = async (req, res, next) => {
  try {
    console.log('Validation Input');
    await CreateBusinessSchema.validateAsync(req.body);
    console.log('Attemping to create business entry');
    const result = await Business.Create(req.body);
    console.log('Business entry created');
    return res.status(201).json(result);
  } catch (error) {
    console.error('Error Creating Business');
    if (error instanceof ValidationError) {
      return next(
        new RequestError({
          status: 400,
          message: error.message,
        }),
      );
    }
    return next(error);
  }
};

export default CreateBusinessRequestHandler;
