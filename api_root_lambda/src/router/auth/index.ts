import { RequestHandler, Router } from 'express';
import { GetCurrentUserId } from '../../util/request';

const AuthRouter = Router();

const AuthRouteHandler : RequestHandler = (_, res) => res.status(200).json({
  userId: GetCurrentUserId(),
});

AuthRouter.route('/who-am-i').get(AuthRouteHandler).post(AuthRouteHandler);
// AuthRouter.route('/business').post(CreateBusinessRequestHandler);

export default AuthRouter;
