import { Router } from 'express';
import echoRouter from './echo';
import businessRouter from './business';
import userRouter from './user';

const PublicRouter = Router();

PublicRouter.use('/echo', echoRouter);

PublicRouter.use('/business', businessRouter);

PublicRouter.use('/user', userRouter);

export default PublicRouter;
