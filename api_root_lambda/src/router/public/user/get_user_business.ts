import { RequestHandler } from 'express';
import Business from '../../../model/business';

const RetrieveUserBusinessHandler : RequestHandler = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const businesses = await Business.GetBusinessesForUser(userId);
    return res.status(200).json({ businesses });
  } catch (error) {
    return next(error);
  }
};

export default RetrieveUserBusinessHandler;
