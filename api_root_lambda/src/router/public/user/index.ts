import { Router } from 'express';
import RetrieveUserBusinessHandler from './get_user_business';

const UserRouter = Router();

UserRouter.route('/id/:userId/businesses').get(RetrieveUserBusinessHandler);

export default UserRouter;
