import { RequestHandler } from 'express';
import Analytics from '../../../analytics';

const GetTopBusinessRequestHandler : RequestHandler = async (req, res, next) => {
  try {
    const { count } = req.query;
    const limit = count ? parseInt(count as string, 10) : 5;
    const businesses = await Analytics.TopBusinesses(limit);
    return res.status(200).json({
      businesses,
    });
  } catch (error) {
    return next(error);
  }
};

export default GetTopBusinessRequestHandler;
