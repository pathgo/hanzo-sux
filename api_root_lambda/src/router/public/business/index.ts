import { Router } from 'express';
import CreateBusinessRequestHandler from '../../auth/create_business.handler';
import handleFindBusinessbyId from './search';
import handleUpdateFloorplan from './update_floorplan.handler';
import PathFinderRouteHandler from './path_finder.handler';
import FindBusinessByZipHandler from './search_by_zip.handler';
import HandleResetAnalyticRequest from './reset_analytics.handler';
import GetTopBusinessRequestHandler from './get_top_businesses.handler';
import DeleteBusinessRequestHandler from './delete_business';

const router = Router();

router.route('/').post(CreateBusinessRequestHandler);

router.route('/search')
  .get(handleFindBusinessbyId);

router.route('/id/:businessId/floorPlan')
  .post(handleUpdateFloorplan);

router.route('/id/:businessId')
  .delete(DeleteBusinessRequestHandler);

router.route('/id/:businessId/get-path')
  .get(PathFinderRouteHandler);

router.route('/analytics/hits/reset').post(HandleResetAnalyticRequest);

router.route('/analytics/hits/top').get(GetTopBusinessRequestHandler);

router.route('/zip/getbyzip')
  .get(FindBusinessByZipHandler);

export default router;
