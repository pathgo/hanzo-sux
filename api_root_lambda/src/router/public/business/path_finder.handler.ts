/* eslint-disable global-require */
import { RequestHandler } from 'express';
import Business from '../../../model/business';
import ShortestPath from '../../../util/pathfinder';

const PathFinderRouteHandler: RequestHandler = async (req, res, next) => {
  try {
    const srow = parseInt(req.query.start_row as string, 10);
    const scol = parseInt(req.query.start_col as string, 10);
    const erow = parseInt(req.query.end_row as string, 10);
    const ecol = parseInt(req.query.end_col as string, 10);
    const { businessId } = req.params;
    const business = await Business.FindBusinessById(businessId);
    const floorPlantoUse = business.floorPlan[0].floorGrid;
    const userpath = ShortestPath(floorPlantoUse, [srow, scol], [erow, ecol]);
    return res.status(200).json({
      userpath,
    });
  } catch (error) {
    return next(error);
  }
};

export default PathFinderRouteHandler;
