import { RequestHandler } from 'express';
import RequestError from '../../../error/request_error';
import Business from '../../../model/business';
import Analytics from '../../../analytics';
import logger from '../../../util/logger';

const LogBusinessSearched = async (businessId: string) : Promise<void> => {
  const TAG = LogBusinessSearched.name;
  try {
    await Analytics.LogBusinessHit(businessId);
  } catch (error) {
    logger.info(TAG, 'Unable to log business visited');
    logger.info(TAG, `Business ID: ${businessId}`);
    logger.info(TAG, error);
  }
};

const FindBusinessRequestHandler: RequestHandler = async (req, res, next) => {
  try {
    const {
      id,
      title,
    } = req.query;
    if (id) {
      const business = await Business.FindBusinessById(id as string);
      await LogBusinessSearched(business.id);
      return res.status(200).json(business);
    }
    if (title) {
      const businessList = await Business.FindByTitle(title as string);
      await Promise.all(
        businessList.map((business) => LogBusinessSearched(business.id)),
      );
      return res.status(200).json({
        businessList,
      });
    }
    return next(new RequestError({
      status: 400,
      message: 'Query Param (title | id) is required',
    }));
  } catch (error) {
    return next(error);
  }
};

export default FindBusinessRequestHandler;
