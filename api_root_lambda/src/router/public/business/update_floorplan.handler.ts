import pako from 'pako';
import { RequestHandler } from 'express';
import RequestError from '../../../error/request_error';
import FloorPlan from '../../../model/floor_plan';
import { FloorPlanModel } from '../../../model/floor_plan/types';

const handleUpdateFloorplan : RequestHandler = async (req, res, next) => {
  try {
    const { businessId } = req.params;
    const { floorPlan: compressedFloorPlan } = req.body;
    if (compressedFloorPlan === undefined) {
      throw new RequestError({
        status: 400,
        message: 'floorPlan is required',
      });
    }
    const floorPlan = JSON.parse(pako.inflate(compressedFloorPlan, { to: 'string' })) as FloorPlanModel;
    await FloorPlan.UpdateFloorplan(businessId, floorPlan);
    return res.status(200).json({
      message: `Floorplan for Business(${businessId}) has been updated`,
    });
  } catch (error) {
    return next(error);
  }
};

export default handleUpdateFloorplan;
