import { RequestHandler } from 'express';
import RequestError from '../../../error/request_error';
import Business from '../../../model/business';

const FindBusinessByZipHandler: RequestHandler = async (req, res, next) => {
  try {
    const zipcode = parseInt(req.query.zip as string, 10);

    if (zipcode) {
      const businesses = await Business.FindBusinessByZip(zipcode as number);
      return res.status(200).json({
        businesses,
      });
    }
    return next(new RequestError({
      status: 400,
      message: 'Query Param zip is required',
    }));
  } catch (error) {
    return next(error);
  }
};

export default FindBusinessByZipHandler;
