import { RequestHandler } from 'express';
import Business from '../../../model/business';

const DeleteBusinessRequestHandler : RequestHandler = async (req, res, next) => {
  try {
    const { businessId } = req.params;
    await Business.DeleteBusiness(businessId);
    return res.status(200).json({
      message: 'Success',
    });
  } catch (error) {
    return next(error);
  }
};

export default DeleteBusinessRequestHandler;
