import { RequestHandler } from 'express';
import Analytics from '../../../analytics';

const HandleResetAnalyticRequest : RequestHandler = async (_, res, next) => {
  try {
    await Analytics.ResetBusinessHits();
    return res.status(200).json({
      message: 'Done',
    });
  } catch (error) {
    return next(error);
  }
};

export default HandleResetAnalyticRequest;
