import CreateExpressApp from '../app';
import PublicRouter from '../router/public';

const publicApp = CreateExpressApp(PublicRouter);

const PORT = 3000;

publicApp.listen(PORT, () => {
  console.log(`Public Server running on port ${PORT}`);
});
