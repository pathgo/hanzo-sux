import AppStage, { CURRENT_APP_STAGE } from '../constant/app_stage';
import logger from '../util/logger';
import { GetENVOrThrow } from '../util/setup';

const LOG_TAG = 'FirebaseConfig';

const GetCredential = () => {
  if (CURRENT_APP_STAGE === AppStage.Test) {
    return {
      projectId: '',
      clientEmail: '',
      privateKey: '',
    };
  }
  let privateKey = '';
  logger.info(LOG_TAG, 'Using Credential from default');
  if (process.env.FB_PRIVATE_KEY__BASE64) {
    logger.info(LOG_TAG, 'Using Base64');
    privateKey = Buffer.from(process.env.FB_PRIVATE_KEY__BASE64, 'base64').toString('ascii');
  } else {
    privateKey = GetENVOrThrow('FB_PRIVATE_KEY');
  }
  return {
    projectId: GetENVOrThrow('FB_PROJECT_ID'),
    clientEmail: GetENVOrThrow('FB_CLIENT_EMAIL'),
    privateKey,
  };
};

const FirebaseConfig = {
  Credential: GetCredential(),
  CollectionName: {
    Business: 'BusinessV2',
  },
};

export default FirebaseConfig;
