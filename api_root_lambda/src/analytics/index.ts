import { firestore } from 'firebase-admin';
import FirebaseConfig from '../config/firebase';
import { BusinessModelSummary, FirebaseBusinessFloorPlanCollection } from '../model/business/types';
import Firestore from '../service/firebase/firestore';

export default class Analytics {
  static async LogBusinessHit(businesId: string) : Promise<void> {
    const doc = Firestore.collection(FirebaseConfig.CollectionName.Business).doc(businesId);
    await doc.update({ visited: firestore.FieldValue.increment(1) });
  }

  static async TopBusinesses(count = 5) : Promise<Array<BusinessModelSummary>> {
    const results = await Firestore.collection(FirebaseConfig.CollectionName.Business)
      .orderBy('visited', 'desc')
      .limit(count)
      .get();
    const businesses = results.docs.map(
      (entry) => {
        const fbDoc = entry.data() as Omit<FirebaseBusinessFloorPlanCollection, 'id'>;
        const model: BusinessModelSummary = {
          id: entry.id,
          businessName: fbDoc.businessName,
          address: {
            ...fbDoc.address,
            zipcode: fbDoc.zipcode,
          },
        };
        return model;
      },
    );
    return businesses;
  }

  static async ResetBusinessHits() : Promise<void> {
    const documents = await Firestore.collection(FirebaseConfig.CollectionName.Business).get();
    await Promise.all(documents.docs.map((doc) => doc.ref.update({
      visited: 0,
    })));
  }
}
