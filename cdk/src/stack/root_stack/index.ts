import {
  AccessLogFormat,
  AuthorizationType,
  CognitoUserPoolsAuthorizer,
  Cors,
  LambdaIntegration,
  LogGroupLogDestination,
  MethodLoggingLevel,
  RestApi,
} from '@aws-cdk/aws-apigateway';
import {
  UserPool,
  AccountRecovery,
  VerificationEmailStyle,
  IUserPool,
} from '@aws-cdk/aws-cognito';
import {
  Code,
  Function,
  FunctionProps,
  IFunction,
  Runtime,
} from '@aws-cdk/aws-lambda';
import * as cdk from '@aws-cdk/core';
import { Duration, StackProps } from '@aws-cdk/core';
import * as dotenv from 'dotenv';
import { LogGroup, RetentionDays } from '@aws-cdk/aws-logs';
import AppStage from '../../constant/app_stage';
import { resourceName } from '../../util/resource';
import { GetENVOrThrow } from '../../util/setup';

dotenv.config();

export interface RootStackProps extends StackProps {
  stage: AppStage;
}

export default class RootStack extends cdk.Stack {
  public restAPI: RestApi;

  public publicAPIHandler: IFunction;

  public protectedAPIHandler: IFunction;

  private stage: AppStage;

  private userPool: UserPool;

  constructor(scope: cdk.Construct, id: string, props: RootStackProps) {
    super(scope, id, props);
    this.stage = props.stage;
    const resourceNames = {
      apiGateway: this.resName('APIGateway'),
      publicAPIHandler: this.resName('PublicAPIHandler'),
      authAPIHandler: this.resName('AuthAPIHandler'),
      userPool: this.resName('GatewayUserPool'),
    };

    // ============ API Handler Lambdas ============

    const apiHandlerSharedProps: Omit<FunctionProps, 'handler'> = {
      runtime: Runtime.NODEJS_12_X,
      code: Code.fromAsset('./build/api_root_lambda'),
      timeout: Duration.seconds(15),
      environment: this.apiHandlerEnv(),
    };

    this.publicAPIHandler = new Function(this, resourceNames.publicAPIHandler, {
      ...apiHandlerSharedProps,
      handler: 'index.PublicAPIHandler',
      logRetention: RetentionDays.ONE_WEEK,
      functionName: resourceNames.publicAPIHandler,
    });

    this.protectedAPIHandler = new Function(
      this,
      resourceNames.authAPIHandler,
      {
        ...apiHandlerSharedProps,
        handler: 'index.ProtectedAPIHandler',
        logRetention: RetentionDays.ONE_WEEK,
        functionName: resourceNames.authAPIHandler,
      },
    );

    // ============ API ============
    this.restAPI = new RestApi(this, resourceNames.apiGateway, {
      defaultCorsPreflightOptions: {
        allowOrigins: Cors.ALL_ORIGINS,
        allowMethods: Cors.ALL_METHODS,
        allowHeaders: Cors.DEFAULT_HEADERS,
      },
      deployOptions: {
        loggingLevel: MethodLoggingLevel.INFO,
        accessLogDestination: new LogGroupLogDestination(
          new LogGroup(this, this.resName('APILogGroup')),
        ),
        accessLogFormat: AccessLogFormat.jsonWithStandardFields(),
        throttlingRateLimit: 30,
        throttlingBurstLimit: 50,
      },
    });
    this.restAPI.root.addMethod('any');

    // ============ Public API ============

    const publicEndpoint = this.restAPI.root.addResource('public', {
      defaultIntegration: new LambdaIntegration(this.publicAPIHandler, {
        proxy: true,
      }),
    });
    publicEndpoint.addMethod('any');
    publicEndpoint.addProxy({ anyMethod: true });

    // ============ Auth API ============

    this.userPool = this.createUserPool();
    const authorizer = this.createCognitouserPoolAuthroizer(this.userPool);
    const authEndpoint = this.restAPI.root.addResource('auth', {
      defaultIntegration: new LambdaIntegration(this.protectedAPIHandler, {
        proxy: true,
      }),
      defaultMethodOptions: {
        authorizationType: AuthorizationType.COGNITO,
        authorizer,
      },
    });
    authEndpoint.addMethod('any');
    authEndpoint.addProxy();
  }

  apiHandlerEnv = (): Record<string, string> => {
    const environment: Record<string, string> = {
      FB_PROJECT_ID: GetENVOrThrow('FB_PROJECT_ID'),
      FB_CLIENT_EMAIL: GetENVOrThrow('FB_CLIENT_EMAIL'),
    };
    if (process.env.FB_PRIVATE_KEY) environment.FB_PRIVATE_KEY = process.env.FB_PRIVATE_KEY;
    if (process.env.FB_PRIVATE_KEY__BASE64) {
      environment.FB_PRIVATE_KEY__BASE64 = process.env.FB_PRIVATE_KEY__BASE64;
    }
    return environment;
  };

  createUserPool = (): UserPool => {
    const name = this.resName('UserPool');
    const userPool = new UserPool(this, name, {
      userPoolName: name,
      signInAliases: {
        email: true,
      },
      selfSignUpEnabled: true,
      accountRecovery: AccountRecovery.EMAIL_AND_PHONE_WITHOUT_MFA,
      userVerification: {
        emailStyle: VerificationEmailStyle.LINK,
      },
    });
    userPool.addDomain(resourceName('userpool-domain', this.stage), {
      cognitoDomain: {
        domainPrefix: resourceName('userpool', this.stage).toLowerCase(),
      },
    });
    userPool.addClient('MobileAppClient');
    return userPool;
  };

  createCognitouserPoolAuthroizer = (
    userPool: IUserPool,
  ): CognitoUserPoolsAuthorizer => new CognitoUserPoolsAuthorizer(this, this.resName('cognito-authrizer'), {
    cognitoUserPools: [userPool],
  });

  private resName = (res: string): string => resourceName(res, this.stage);
}
