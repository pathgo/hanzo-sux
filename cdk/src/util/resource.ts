import AppStage from '../constant/app_stage';

export const resourceName = (
  resource: string,
  stage: AppStage,
) : string => `PathGo--${resource}-${stage}${process.env.USER ? `-${process.env.USER}` : ''}`;
